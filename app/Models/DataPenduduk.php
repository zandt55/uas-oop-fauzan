<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPenduduk extends Model
{
    use HasFactory;

    protected $fillable = [
        'nik',
        'nama',
        'ttl',
        'gender',
        'alamat',
        'agama',
        'pekerjaan',
        'kewarganegaraan',
        'berlaku',
        'foto'
    ];
}