<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          Tambah Data Penduduk.
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="post" action="{{route('dapen.store')}}">
                    @csrf
                        <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                            <div class="grid grid-cols-3 gap-6">
                                <div class="col-span-3 sm:col-span-2">
                                    <label class="block text-sm font-medium text-gray-700">
                                        NIK
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input type="text" name="nik" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300 @error('title') border-red-500 @enderror" placeholder="NIK">
                                    </div>
                                    @error('nik')
                                        <div class="text-red-600">{{$message}}</div>
                                    @enderror
                                </div>
                                
                                <div class="col-span-3 sm:col-span-2 pt-4">
                                    <label class="block text-sm font-medium text-gray-700">
                                        NAMA LENGKAP
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input type="text" name="nama" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300 @error('title') border-red-500 @enderror" placeholder="Nama Lengkap">
                                    </div>
                                    @error('nama')
                                        <div class="text-red-600">{{$message}}</div>
                                    @enderror
                                </div>
                                
                                <div class="col-span-3 sm:col-span-2 pt-4">
                                    <label class="block text-sm font-medium text-gray-700">
                                        TEMPAT TANGGAL LAHIR
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input type="date" name="ttl" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300 @error('title') border-red-500 @enderror" placeholder="Tempat Tanggal Lahir">
                                    </div>
                                    @error('ttl')
                                        <div class="text-red-600">{{$message}}</div>
                                    @enderror
                                </div>
                                
                                <div class="col-span-3 sm:col-span-2 pt-4">
                                    <label class="block text-sm font-medium text-gray-700">
                                        JENIS KELAMIN
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input 
                                          type="text" name="gender"  placeholder="Jenis Kelamin"
                                          class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300 @error('title') border-red-500 @enderror"
                                        >
                                    </div>
                                    @error('gender')
                                        <div class="text-red-600">{{$message}}</div>
                                    @enderror
                                </div>
                                
                                <div class="col-span-3 sm:col-span-2 pt-4">
                                    <label class="block text-sm font-medium text-gray-700">
                                        ALAMAT
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input 
                                          type="text" name="alamat"  placeholder="Alamat Lengkap"
                                          class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300 @error('title') border-red-500 @enderror"
                                        >
                                    </div>
                                    @error('alamat')
                                        <div class="text-red-600">{{$message}}</div>
                                    @enderror
                                </div>
                                
                                <div class="col-span-3 sm:col-span-2 pt-4">
                                    <label class="block text-sm font-medium text-gray-700">
                                        AGAMA
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input 
                                          type="text" name="agama"  placeholder="Agama"
                                          class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300 @error('title') border-red-500 @enderror"
                                        >
                                    </div>
                                    @error('agama')
                                        <div class="text-red-600">{{$message}}</div>
                                    @enderror
                                </div>
                                
                                <div class="col-span-3 sm:col-span-2 pt-4">
                                    <label class="block text-sm font-medium text-gray-700">
                                        PEKERJAAN
                                    </label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input 
                                          type="text" name="pekerjaan"  placeholder="Pekerjaan"
                                          class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300 @error('title') border-red-500 @enderror"
                                        >
                                    </div>
                                    @error('pekerjaan')
                                        <div class="text-red-600">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                          <a href="{{ route('dapen.create') }}" class="btn btn-xs btn-info pull-right">
                            <x-button class="mt-8" type="submit">
                                Submit
                            </x-button>
                          </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="py-12 px-20">

        

    </div>
</x-app-layout>