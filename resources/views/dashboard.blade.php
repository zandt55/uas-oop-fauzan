<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            UAS OOP - Fauzan Abdurrahman
        </h2>
    </x-slot>

    <style>

    </style>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <a href="{{ route('dapen.create') }}" class="btn btn-xs btn-info pull-right">
                        <x-button class="mb-4">
                            Tambah Data
                        </x-button>
                    </a>
                    <table class="border-collapse table-auto w-full text-sm">
                        <thead>
                            <tr>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    NIK
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    NAMA
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    TEMPAT/TGL LAHIR
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    JENIS KELAMIN
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    ALAMAT
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    AGAMA
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    STATUS PERKAWINAN
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    PEKERJAAN
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    KEWARGANEGARAAN
                                </th>
                                <th class="border-b border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 text-slate-200 text-left">
                                    BERLAKU HINGGA
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dapen as $val) 
                                <tr>
                                    <td>{{ $val->nik }}</td>
                                    <td>{{ $val->nama }}</td>
                                    <td>{{ $val->ttl }}</td>
                                    <td>{{ $val->gender }}</td>
                                    <td>{{ $val->alamat }}</td>
                                    <td>{{ $val->agama }}</td>
                                    <td>-</td>
                                    <td>{{ $val->pekerjaan }}</td>
                                    <td>{{ $val->kewarganegaraan }}</td>
                                    <td>{{ $val->berlaku }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
