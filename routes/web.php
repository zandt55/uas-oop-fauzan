<?php

use App\Http\Controllers\DapenController;
use App\Models\DataPenduduk;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    $dapen = DataPenduduk::get();
    // dd($dapen);
    return view('dashboard', compact('dapen'));

})->middleware(['auth'])->name('dashboard');

Route::resource('/dapen', \App\Http\Controllers\DapenController::class);

// Route::post('tambah-data-penduduk', [DapenController::class, 'store'])->name('dapen.store');

require __DIR__.'/auth.php';
