<?php

namespace Database\Seeders;

use App\Models\DataPenduduk;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DapenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DataPenduduk::factory(10)->create();
    }
}
