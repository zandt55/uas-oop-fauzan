<?php

namespace Database\Factories;

use App\Models\DataPenduduk;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class DapenFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = DataPenduduk::class;

    public function definition()
    {
        return [
            'nik' => $this->faker->nik(),
            'nama' => $this->faker->nama,
            'ttl' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'gender' => $this->faker->gender,
            'alamat' => $this->faker->address,
            'agama' => $this->faker->word,
            'pekerjaan' => $this->faker->jobTitle,
            'kewarganegaraan' => "WNI",
            'berlaku' => "Seumur Hidup",
            'foto' => "/foto.png"
        ];
    }
}
